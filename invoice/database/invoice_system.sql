-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2021 at 05:54 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone_number` bigint(25) NOT NULL,
  `gst_number` varchar(25) NOT NULL,
  `address` varchar(250) NOT NULL,
  `place_of_supply` varchar(250) NOT NULL,
  `state_code` varchar(250) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `delete_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `client_taxes`
--

CREATE TABLE `client_taxes` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `tax_name` varchar(50) NOT NULL,
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `company_full_title` varchar(250) NOT NULL,
  `website_address` varchar(50) NOT NULL,
  `mail_address` varchar(50) NOT NULL,
  `company_address` varchar(500) NOT NULL,
  `CIN_number` varchar(50) NOT NULL,
  `PAN_number` varchar(50) NOT NULL,
  `GST_number` varchar(50) NOT NULL,
  `IEC_number` varchar(50) NOT NULL,
  `Bank_name` varchar(50) NOT NULL,
  `INR_account_number` int(50) NOT NULL,
  `EURO_account_number` int(50) NOT NULL,
  `Routing_number` int(50) NOT NULL,
  `swift_code` varchar(50) NOT NULL,
  `mobile_number` int(11) NOT NULL,
  `hsn_no` bigint(20) NOT NULL,
  `note_1` varchar(250) NOT NULL,
  `note_2` varchar(250) NOT NULL,
  `note_3` varchar(250) NOT NULL,
  `note_4` varchar(250) NOT NULL,
  `note_5` varchar(250) NOT NULL,
  `thank_you_message` varchar(250) NOT NULL,
  `login_email` varchar(50) NOT NULL,
  `login_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`first_name`, `last_name`, `company_full_title`, `website_address`, `mail_address`, `company_address`, `CIN_number`, `PAN_number`, `GST_number`, `IEC_number`, `Bank_name`, `INR_account_number`, `EURO_account_number`, `Routing_number`, `swift_code`, `mobile_number`, `hsn_no`, `note_1`, `note_2`, `note_3`, `note_4`, `note_5`, `thank_you_message`, `login_email`, `login_password`) VALUES
('DISHA', 'JOSHI', 'Weboccult Technologies Pvt. Ltd.', 'www.weboccult.com', 'info@weboccult.com', 'Akik Tower, 402-403 & 502-503, near Pakwan Dining Hall, Bodakdev, Ahmedabad, Gujarat 380054', 'U72900GJ2019PTC107851', 'AACCW4213A', '24AACCW4213A1Z1', 'AACCW4213A', 'HDFC BANK', 2147483647, 2147483647, 26009593, 'HDFCINBB', 2147483647, 998314, 'AMOUNT OF THE BILL TO PAID BEFORE DUE DATE', 'PAY ONLY IN MENTIONED CURRENCY IN INVOICE', 'SUBJECT TO JUNSDICTION AHEMDABAD', ' ', ' ', 'THANK YOU FOR YOUR BUSINESS', 'wot@gmail.com', 'wot@404');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_date` varchar(25) NOT NULL,
  `due_date` varchar(25) NOT NULL,
  `client_name` varchar(25) NOT NULL,
  `project_ids` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `delete_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `project_description` varchar(1000) NOT NULL,
  `is_hourly_based` int(25) NOT NULL,
  `rate_per_hour` varchar(11) NOT NULL,
  `delete_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone_number` bigint(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone_number`, `password`) VALUES
(1, 'weboccult', 'weboccult@gmail.com', 79, 'weboccult_123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_taxes`
--
ALTER TABLE `client_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_description` (`project_description`) USING HASH;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_taxes`
--
ALTER TABLE `client_taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
