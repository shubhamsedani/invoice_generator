<?php session_start(); session_destroy(); ?>

<html>
<head>
<link rel="stylesheet" href="assets/css/style.css">
<style>
#username_none, #password_none{display: none; color:red;}
body { background: #97caef !important; } 
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
</head>

<div class="login-page">
  <div class="form style final">
    <div class="login-form">  
    <div class="title">
      <img src="assets/images/invoice.png">
      <h1>Invoice System</h1>
      <h4>Sign in to access the system</h4>
    </div>        
      <input type="text" placeholder="User Name" id="UserName" required>
      <div id="username_none" class="alert alert-danger">Enter user name</div>
      <input type="password" placeholder="Password" id="Password" required>
      <div id="password_none" class="alert alert-danger">Enter Password</div>
      <br><br>
      <button style="max-width: 380px" onclick="log()">Sign in</button>     
    </div>
  </div>
</div>

<script>
  $('.message a').click(function(){
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
  });

  function log(){

    if($("#UserName").val().length === 0 ){
      $("#username_none").css("display", "block");
    }else if($("#Password").val().length === 0 ){
      $("#username_none").css("display", "none");
      $("#password_none").css("display", "block");
    }else{
        var VUserName = $("#UserName").val();
        var VPassword = $("#Password").val();
        $.ajax({
            type: 'POST',
            url: 'backend/auth/login.php',
            data: { UserName:VUserName,Password:VPassword },
            dataType:'json',
            success: function(response) {
            	if(response.status=="1")
                	window.location.href = "views/dashboard.php";
                else
                	alert("Wrong credentials !!!");
            }
        });
    }
  }

</script>
</html>