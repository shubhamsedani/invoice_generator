<?php 
require('assets/TCPDF/tcpdf.php');
include "conf/conn.php";
// echo "<pre>";
// print_r($_POST);
// echo "</pre>";
$selectquery = "select * FROM company_details";
                  $query = mysqli_query($conn, $selectquery);
                  $result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
                  if (is_array($result_arr) || is_object($result_arr))
                  {
                     foreach ($result_arr as $row) {
 
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak; 
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        // $img_file = __dir__.'./assets/images/background.jpg';
        // $this->Image($img_file, 0, 0, 420, 270, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

function test(float $number){
   $no = floor($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'One', '2' => 'Two',
    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
    '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
    '13' => 'Thirteen', '14' => 'Fourteen',
    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
    '60' => 'Sixty', '70' => 'Seventy',
    '80' => 'Eighty', '90' => 'Ninety');
   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
          // return $result . "Rupees  " . $points . " Paise";
  return $result . "Rupees  ";
}

$tcpdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$tcpdf->AddPage('P', 'A4');

$pdf = '<table border="1" cellpadding="5px">
            <tbody>
                <tr>   
                 <td rowspan="2"></td>
                 <td colspan="4" style="font-size:30px; text-align: center; vertical-align: middle;">'.$row["company_full_title"].'</td>
                 <td style="text-align: center; vertical-align: middle;"><h1>INVOICE</h1></td>   
               </tr>
               <tr>
                 <td colspan="2" style=" text-align: center; vertical-align: middle;">Web: <span style="color: #0000EE;">'.$row["website_address"].'</span></td>
                 <td colspan="2" style="text-align: center; vertical-align: middle;">Mail: '.$row["mail_address"].'</td>
                 <td style=" background-color:#E8E8E8; text-align: center; vertical-align: middle;"><h4>TAX Invoice</h4></td>   
               </tr>
            </tbody>
</table>';

$pdf .= ' <div class="space"></div>
			
        <table border="1" cellpadding="5px">
            <tbody>
                <tr>
                    <td colspan="7"><h2>Company Address:</h2></td>   
                </tr>
                <tr>
                 <td rowspan="4" colspan="3" style="font-size:16px;">'.$row["company_address"].'</td>   
                 <td rowspan="4" colspan="4"></td>
               </tr>
               
               <tr>
                 <td></td>
                 <td></td>
                 <td></td>    
               </tr>
               <tr>
                 <td></td>
                 <td></td>
                 <td></td>    
               </tr>
            </tbody>
</table>
        ';

$pdf .= '<div class="space"></div>
 <table border="1" cellpadding="5">
    <tr>
        <td style="font-size:14px; background-color:#E8E8E8;">CIN Number:</td>
        <td>'.$row["CIN_number"].'</td>
        <td style="font-size:14px; background-color:#E8E8E8;">Invoice No.:</td>
        <td style="font-size:14px;">'.$_POST["inovice_number"].'</td>
    </tr>
    <tr>
        <td style="font-size:14px; background-color:#E8E8E8;">Company PAN Number:</td>
        <td>'.$row["PAN_number"].'</td>
        <td style="font-size:14px; background-color:#E8E8E8;">Invoice Date:</td>
        <td style="font-size:14px;">'.$_POST["invoice_date"].'</td>
    </tr>
    <tr>
        <td style="font-size:14px; background-color:#E8E8E8;">Company GST Number:</td>
        <td>'.$row["GST_number"].'</td>
        <td style="font-size:14px; background-color:#E8E8E8;">Due Date:</td>
        <td style="font-size:14px;">'.$_POST["due_date"].'</td>
    </tr>
    <tr>
        <td style="font-size:14px; background-color:#E8E8E8;">Company IEC Number:</td>
        <td>'.$row["IEC_number"].'</td>
        <td style="font-size:14px;"></td>
        <td style="font-size:14px;"></td>
    </tr>
</table>';

$client_id = $_POST['client_id'];
$selectquery1 = "select name, email, currency, email, gst_number FROM clients where client_id ='" . $client_id . "'";
$result1 = $conn->query($selectquery1);
while ($row1 = $result1->fetch_assoc()) {


$pdf .= '<div class="space"></div>
 <table border="1" cellpadding="5">
    <tr>
        <td style="font-size:14px; background-color:#E8E8E8;">BILL TO</td>
        <td style="font-size:14px; background-color:#E8E8E8;">ADDRESS</td>
        <td style="font-size:14px; background-color:#E8E8E8;">PLACE OF SUPPLY</td>
        <td style="font-size:14px; background-color:#E8E8E8;">STATE CODE</td>
    </tr>
    <tr>
        <td rowspan="2" style="font-size:14px;">'.$row1['name']." <br>". $row1['email']. "<br>GST No: <br>". $row1['gst_number'].'</td>
        <td rowspan="2" style="font-size:14px;">'.$_POST["address"].'</td>
        <td rowspan="2" style="font-size:14px;">'.$_POST["place_of_supply"].'</td>
        <td rowspan="2" style="font-size:14px;">'.$_POST["code"].'</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>';

$pdf .= ' <div class="space"></div>
            
        <table border="1" cellpadding="5px">
            <tbody>
                <tr style="background-color:#E8E8E8;">
                    <th>No.</th>
                    <th>SAC/HSN</th>
                    <th colspan="2">Project Description</th>
                    <th>Hours</th>
                    <th>Rate</th>
                    <th>Amount</th>   
                </tr>';

for($i = 0; $i<count($_POST['project_id']); $i++){
    $pdf .= '<tr><td>'.($i+1).'</td>';
    $pdf .= '<td>'.$row['hsn_no'].'</td>';
    $pdf .= '<td colspan="2">'.$_POST['project_description'][$i].'</td>';
    $pdf .= '<td>'.$_POST['hours_count'][$i].'</td>';
    $pdf .= '<td>'. $row1['currency'] ." ".$_POST['rate'][$i].'/-</td>';
    $pdf .= '<td>'. $row1['currency'] ." ". $_POST['total'][$i].'/-</td></tr>';
}

for($j = 0; $j<=count($_POST['tax_name']); $j++){
    if($j != count($_POST['tax_name'])){
        $pdf .= '<tr><td></td>';
        $pdf .= '<td></td>';
        $pdf .= '<td colspan="2"></td>';
        $pdf .= '<td>'.$_POST['tax_name'][$j].'</td>';
        $pdf .= '<td>'.$_POST['tax_percentage'][$j].'%</td>';
        $pdf .= '<td>'. $row1['currency'] ." ".$_POST['tax_total'][$j].'/-</td></tr>';
    }else{
        $pdf .= '<tr style="background-color:#E8E8E8;">
                <td colspan="6"><b>Total Invoice Amount:</b></td>';
        $pdf .= '<td>'. $row1['currency'] ." ".$_POST['total_with_tax'].'/-</td></tr>';
    }
}

$pdf .= '<tr><td colspan="7" style="background-color:#E8E8E8;"><b>Amount in words: </b>'.test($_POST['total_with_tax']).'</td></tr>';

$pdf .= '<tr><td colspan="7" style="background-color:#E8E8E8;"><b>Payment Milestone: </b>Montly payment upon invoice</td></tr>';

$pdf .= '</tbody></table>';

$pdf .= ' <div class="space"></div>
            
        <table border="1" cellpadding="5px">
            <tbody>
                <tr>
                    <td colspan="6"><b>Bank Details: '.$row["company_full_title"].'</b></td>   
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">Bank Name:</td>
                    <td style="font-size:14px;">'.$row["Bank_name"].'</td>
                    <td colspan="4" style="font-size:14px;"></td>
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">INR Acc. No:</td>
                    <td style="font-size:14px;">'.$row["INR_account_number"].'</td>
                    <td colspan="4" style="font-size:14px; text-align: center; vertical-align: middle;"><b>Note</b></td>
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">Euro Acc. No:</td>
                    <td style="font-size:14px;">'.$row["EURO_account_number"].'</td>
                    <td colspan="4" style="font-size:14px; text-align: center;">'.$row["note_1"].'</td>
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">Rountig No:</td>
                    <td style="font-size:14px;">'.$row["Routing_number"].'</td>
                    <td colspan="4" style="font-size:14px; text-align: center;">'.$row["note_2"].'</td>
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">Swift Code:</td>
                    <td style="font-size:14px;">'.$row["swift_code"].'</td>
                    <td colspan="4" style="font-size:14px; text-align: center;">'.$row["note_3"].'</td>
                </tr>
                <tr>
                    <td style="font-size:14px; background-color:#E8E8E8;">Mobile No:</td>
                    <td style="font-size:14px;">'.$row["mobile_number"].'</td>
                    <td colspan="4" style="font-size:14px;"></td>
                </tr>
            </tbody>
</table>';

$pdf .= '<div class="space"></div><table border="1" cellpadding="5px">
            <tbody>
                <tr>   
                 <td rowspan="4" style="text-align: center; vertical-align: middle;"><br>'.$row["thank_you_message"].'<br><br>'.$row["first_name"]. " " . $row["last_name"] .' | Director</td>
                 <td rowspan="4"><br><br><br></td>
               </tr>
            </tbody>
</table>';





// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set title of pdf
$tcpdf->SetTitle('WebOccult Invoice');



// set header and footer in pdf
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->setListIndentWidth(3);

// set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, 11);

// set image scale factor
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetFont('times', '', 11);

$tcpdf->writeHTML($pdf, true, false, false, false, '');



// Display image on full page
// $tcpdf->Image('./assets/images/background.jpg', 10, 10, 420, 270, 'JPG','', '', true, 200, '', false, false, 0, false, false, true);
$tcpdf->Image('./assets/images/WebOccult.png', 18, 13, 15, 15, 'png','', '', true, 200, '', false, false, 0, false, false, true);
$tcpdf->Image('./assets/images/right_logo.png', 300, 27, 30, 30, 'png','', '', true, 200, '', false, false, 0, false, false, true);

// $tcpdf->setTextRenderingMode($stroke=0, $fill=true, $clip=false);
// $tcpdf->Write(0, 'Fill text', '', 0, '', true, 0, false, false, 0);

//Close and output PDF document
ob_end_clean();
$tcpdf->Output('invoice.pdf', 'I');

//insert into invoice data table
if(isset($_POST["save_download"])){
  $project_ids = implode(",", $_POST["project_id"]);
  $insertquery = "INSERT INTO `invoices`(`client_id`, `email`,`invoice_no`,`invoice_date`, `due_date`, `client_name`, `project_ids`, `amount`,`delete_status`) VALUES ('" . $_POST["client_id"] ."','" . $row1['email'] ."','" . $_POST["inovice_number"] ."','" . $_POST["invoice_date"] ."','" . $_POST["due_date"] ."','" . $row1['name'] ."','" . $project_ids ."','" . $_POST["total_with_tax"] ."', 'not delete')";
  $query = mysqli_query($conn, $insertquery);
}

}}}
?>


