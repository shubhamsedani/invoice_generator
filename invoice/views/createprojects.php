<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
session_start();

$sql = "SELECT client_id, name, email, currency FROM clients";
$result = mysqli_query($conn, $sql);
?>

<html>
<head>
	<title>Create Projects</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<!-- jquery cdn -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<!-- validation -->
	<style>
	label {display:block; width:x; height:y; text-align:left;}
		.error{
			color: red;
		}
	</style>
	<script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>


<div class="createclients-page">
  <div class="form style">
    <div class="login-form">
    	<form id='Createproject' method='post'>
		      <h3><b>create projects</b></h3>

			   
		      <label>Select client name:</label>
		      <select id="client_id" name="client_name" class="client_name" required>
		      	<option value="select client">select client</option>
			    <?php  
					while($row = mysqli_fetch_array($result))  
					{  
					   echo ' <option data-id ="'.$row["currency"].'" data-attr ="'.$row["client_id"].'" value="'.$row["name"].'">'.$row["name"]. ' (' . $row["email"].')</option>';  
					}  
		        ?> 
			  </select>

			  <label>Enter project name:</label>
		      <input type="text" placeholder="Project Name" name='project_name' id="project_name">
		      <label>Enter project description:</label>
		      <input type="text" placeholder="Project description" name='project_description' id="project_description">     

		      <label>Select your work type:</label>
		      <select id="work_type" name="work_type" required>
			    <option value="no"><b>No</b> its not hour based</option>
			    <option value="yes"><b>Yes</b> its hour based</option>
			  </select>

			  <label class="rate_per_hour">Enter hours:</label>
			  <span class="rate_per_hour"></span>
		      <input type="text" placeholder="Rate per hour" name='rate_per_hour' class="rate_per_hour">
		      <br><br>
		      <input type='submit' id="custom_submit" value='Save Projects'>
      </form>   
    </div>
  </div>
</div>

<script src="../assets/js/main.js"></script>

<script>
	$(document).ready(function(){

		$('#client_id').change(function(){
		  $(".rate_per_hour").text("your currency is " + $(this).children('option:selected').data('id'));
		});

		$("#Createproject").validate({
			rules:{
				client_name:{
					required:true
				},
				project_name:{
					required:true
				},
				project_description:{
					required:true
				},
				rate_per_hour:{
					required:true
				},
				client_id:{
					required: true
				}
			}
		})
	})
</script>


</html>