<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM invoices where delete_status = 'not delete'";
$result = mysqli_query($conn, $sql);
session_start();
?>


<html>
<head> 
	<title>Invoices</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
</head>

<body>
	<div class="custom">
		<div ><a href="deleted_invoice.php"><button align="right" class="btn btn-danger" id="add_client">Deleted invoices</button></a></div>
		<?php 
		   if(isset($_SESSION['delete_invoice_status'])){
		   		echo "<span class='alert alert-primary'>" . $_SESSION['delete_invoice_status'] . "</span><br><br>";
		   } 
	    ?>
	  <table id="myTable" border="1px">
	    <thead>
	      <tr>
	      	  <th>NO</th>
	          <th>INVOICE NO</th>
	      	  <th style="display: none;">CLIENT ID</th>
	          <th>CLIENT NAME</th>
	          <th>CLIENT EMAIL</th>
	          <th>INVOICE DATE</th>
	          <th>DUE DATE</th>
	          <th>PROJECTS</th>
	          <th>AMOUNT</th>
	          <th>DELETE</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  
			   echo '  
					   <tr>  
					   		<td>'.$i++.'</td>
					   		<td>'.$row["invoice_no"].'</td>
					   		<td style="display: none;">'.$row["client_id"].'</td> 
					   		<td>'.$row["client_name"].'</td>
					        <td>'.$row["email"].'</td>  
					        <td>'.$row["invoice_date"].'</td>  
					        <td>'.$row["due_date"].'</td>  
					        <td>'.$row["project_ids"].'</td>  
					        <td>'.$row["amount"].'</td>
					        <td><button class="btn btn-danger invoice" id="delete_invoice">delete</button></td> 
					   </tr>  
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>

<script>
$(document).ready( function () {
    $('#myTable').DataTable();

} );
</script>

<script src="../assets/js/main.js"></script>
<?php unset($_SESSION['delete_invoice_status']); ?>
</html>
