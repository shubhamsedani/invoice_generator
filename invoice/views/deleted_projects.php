<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM projects where delete_status = 'deleted'";
$result = mysqli_query($conn, $sql);
session_start();
?>


<html>
<head> 
	<title>deleted projects</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
</head>

<body>
	<div class="custom">
		<div ><button align="right" class="" id="add_client"></button></div>
		<?php 
		   if(isset($_SESSION['delete_status'])){
		   		echo "<span class='alert alert-danger'>" . $_SESSION['delete_status'] . "</span><br><br>";
		   } 
	   ?>
	  <table id="myTable" border="1px">
	    <thead>
	      <tr>
	      	  <th>NO</th>
	      	  <th style="display: none;">CLIENT ID</th>
	          <th>CLIENT NAME</th>
	          <th>PROJECT NAME</th>
	          <th>PROJECT DESCRIPTION</th>
	          <th>IS HOURLY BASED</th>
	          <th>RATE PER HOUR</th>
	          <th>RESTORE</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  
			   echo '  
					   <tr>  
					   		<td>'.$i++.'</td>
					   		<td style="display: none;">'.$row["client_id"].'</td> 
					        <td>'.$row["client_name"].'</td>  
					        <td>'.$row["project_name"].'</td>  
					        <td>'.$row["project_description"].'</td>  
					        <td>'.$row["is_hourly_based"].'</td>  
					        <td>'.$row["rate_per_hour"].'</td>
					        <td><button class="btn btn-warning restore" id="restore">Restore</button></td> 
					   </tr>  
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>

<script>
$(document).ready( function () {
    $('#myTable').DataTable();

} );

document.getElementById('add_client').style.visibility='hidden';
</script>

<script src="../assets/js/main.js"></script>
<?php unset($_SESSION['delete_status']); ?>
</html>
