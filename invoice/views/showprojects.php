<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM projects where delete_status = 'not delete'";
$result = mysqli_query($conn, $sql);
session_start();
?>


<html>
<head> 
	<title>show projects</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
</head>

<body>
	<div class="custom">
		<div >
			<a href="createprojects.php"><button align="right" class="btn btn-primary" id="add_client">Add Projects</button></a>
			<a href="deleted_projects.php"><button align="right" class="btn btn-danger" id="deleted">Deleted Projects</button></a>
		</div>
		<?php 
		   if(isset($_SESSION['project_delete_status'])){
		   		echo "<span class='alert alert-danger'>" . $_SESSION['project_delete_status'] . "</span><br><br>";
		   } 

		    if(isset($_SESSION['editproject_status'])){
		   		echo "<span class='alert alert-danger'>" . $_SESSION['editproject_status'] . "</span><br><br>";
		   }  

			if (isset($_SESSION['project_status'])){
			    echo "<span class='alert alert-danger'>" . $_SESSION['project_status'] . "</span><br><br>";
			}
	   ?>
	  <table id="myTable" border="1px">
	    <thead>
	      <tr>
	      	  <th>NO</th>
	      	  <th style="display: none;">CLIENT ID</th>
	          <th>CLIENT NAME</th>
	          <th>PROJECT NAME</th>
	          <th>PROJECT DESCRIPTION</th>
	          <th>IS HOURLY BASED</th>
	          <th>RATE PER HOUR</th>
	          <th>EDIT</th>
	          <th>DELETE</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  if($row["is_hourly_based"] == "1"){
					$hourbasedvalue = "Yes";
				}else{
					$hourbasedvalue = "No";
				}
			   echo '  
					   <tr>  
					   		<td>'.$i++.'</td>
					   		<td style="display: none;">'.$row["client_id"].'</td> 
					        <td>'.$row["client_name"].'</td>  
					        <td>'.$row["project_name"].'</td>  
					        <td>'.$row["project_description"].'</td>  
					        <td>'.$hourbasedvalue.'</td>  
					        <td>'.$row["rate_per_hour"].'</td>
					        <td><button class="btn btn-warning" id="edit"><a href="editprojects.php?project_description='.base64_encode($row["project_description"]).'">Edit</a></button></td>
					        <td><button class="btn btn-danger" id="delete_project">Delete</button></td>  
					   </tr>  
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>

<script>
$(document).ready( function () {
    $('#myTable').DataTable();

} );
</script>

<?php unset($_SESSION['project_delete_status']); ?>
<?php unset($_SESSION['editproject_status']); ?>
<?php unset($_SESSION['project_status']); ?>
</html>
