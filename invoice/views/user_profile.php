<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
?>

<html>
<head>
   <title>Edit User</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <!-- style CSS -->
   <link rel="stylesheet" href="../assets/css/dashboard.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
   <!-- jquery cdn -->
   <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <!-- jquery validation -->
   <style>
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>

<div ><a href="#"><button align="right" class="btn btn-success" id="mtop_space">Refresh page</button></a></div>
<!-- mtop-space is written to leave space -->

  <div class="form">
    <div class="login-form">
      <?php 
       if(isset($_SESSION['user_update_status'])){
          echo "<span class='alert alert-danger'>" . $_SESSION['user_update_status'] . "</span><br><br>";
       } 
     ?>
           <h3><b>User details</b></h3>
            <!-- <form id="admin_details_invoice" method="post" action="../backend/crud/update_admin.php">  -->
            <form id="user_details">
            <table class="table">
               <?php 
                  $selectquery = "select * FROM users";
                  $query = mysqli_query($conn, $selectquery);
                  $result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
                  if (is_array($result_arr) || is_object($result_arr))
                  {
                     foreach ($result_arr as $row) {
               ?>

                <tbody>
                <tr>
                  <td><h4>User name:</h4></td>
                 <td colspan="6"><input type="text" value="<?=$row['email']?>" name='user_email'></td>
               </tr>
               <tr>
                  <td><h4>Password:</h4></td>
                 <td colspan="6"><input type="text" value="<?=$row['password']?>" name='user_password'></td>
               </tr>
             </tbody>
           <?php }}?>  
      </table>
      <button type="submit" class="btn btn-success" name="update_admin" id="update_admin">Update User</button>
      </form>
  </div>
</div>

<script src="../assets/js/main.js"></script>
<script>
  $("#user_details").submit(function(e){
    e.preventDefault();
    e.stopPropagation(); 
    $.ajax({
           type: "POST",
           url: '../backend/crud/update_user.php',
           dataType:'JSON',
           data: $(this).serialize(),
           success: function( response ){
             if(response.status == 0){
                location.reload();
                  }else{
                      location.reload();
                  }  
            }
        });
});


  $(document).ready(function(){
    $("#user_details").validate({
      rules:{
        user_email:{
          required:true
        },
        user_password:{
          required:true
        }
      }
    })
  })


</script>
<?php unset($_SESSION['user_update_status']); ?>
</html>