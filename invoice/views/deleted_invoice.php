<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM invoices where `delete_status`='deleted'";
$result = mysqli_query($conn, $sql);
?>

<html>
<head> 
	<title>Deleted invoices</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
	<!-- style CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<link rel="stylesheet" href="../assets/css/style.css">
</head>

<body>
	<div class="custom">
		<div ><button align="right" class="btn btn-success" id="add_client"></button></div>
				<?php
				   if(isset($_SESSION['invoice_restore_status'])){
				   		echo "<span class='alert alert-danger'>" . $_SESSION['invoice_restore_status'] . "</span><br><br>";
				   } 
			   ?>
	  <table id="myTable" border="1px">
	    <thead>
	       <tr>
	      	  <th>NO</th>
	      	  <th style="display: none;">CLIENT ID</th>
	          <th>INVOICE NO</th>
	          <th>CLIENT NAME</th>
	          <th>CLIENT EMAIL</th>
	          <th>INVOICE DATE</th>
	          <th>DUE DATE</th>
	          <th>PROJECTS</th>
	          <th>AMOUNT</th>
	          <th>DELETE</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  
			   echo '   
					   <tr>  
					   		<td>'.$i++.'</td>
					   		<td style="display: none;">'.$row["client_id"].'</td> 
					   		<td>'.$row["invoice_no"].'</td>
					   		<td>'.$row["client_name"].'</td>
					        <td>'.$row["email"].'</td>  
					        <td>'.$row["invoice_date"].'</td>  
					        <td>'.$row["due_date"].'</td>  
					        <td>'.$row["project_ids"].'</td>  
					        <td>'.$row["amount"].'</td>
					        <td><button class="btn btn-warning restore_invoice" id="restore_client">Restore</button></td> 
					   </tr> 
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>


<script>
$(document).ready( function () {
    $('#myTable').DataTable({
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: true,
    columnDefs: [
            { width: 100 }
        ],
    fixedColumns: true
  });
});
document.getElementById('add_client').style.visibility='hidden';
</script>

<script src="../assets/js/main.js"></script>
<?php unset($_SESSION['invoice_restore_status']); ?> 
</html>
