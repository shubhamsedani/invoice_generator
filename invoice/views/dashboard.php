<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
?>

<html>
<head>
   <title>Generate Invoice</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <!-- style CSS -->
   <link rel="stylesheet" href="../assets/css/dashboard.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
   <!-- jquery cdn -->
   <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <!-- jquery validation -->
   <style>
   label {display:block; width:x; height:y; text-align:left;}
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>

<div ><a href="#"><button align="right" class="btn btn-success" id="mtop_space">Refresh page</button></a></div>
<!-- mtop-space is written to leave space -->
  <div class="form">
    <div class="login-form">
            <form id="client_details_invoice" method="post" action="../tcpdf.php""> 
               
                  <h3><b>Invoice Details</b></h3>
                  <label>Inovice number:</label>
                  <?php 
                        $selectquery = 'SELECT invoice_no FROM invoices ORDER BY id DESC LIMIT 0, 1';
                        $row1 = mysqli_fetch_assoc($conn->query($selectquery) );
                        $invoice_id = $row1['invoice_no'] + 1;
                  ?>
                  <input type="text" value="<?php echo $invoice_id?>" name='inovice_number'>
                  <label>Inovice date:</label>
                  <input type="text" id="invoice_date" name='invoice_date'>
                  <label>Due date:</label>
                  <input type="text" id="datepicker" name='due_date'>
               

                  <h3><b>Client Details</b></h3>
                  <label>Select Client:</label>
                     <select id="client_id" name="client_id">
                     <option id="test_id" value="select_something">Select Client</option>
                      <?php  
                           $sql = "SELECT * FROM clients where delete_status = 'not delete'";
                           $result = mysqli_query($conn, $sql);

                           while($row = mysqli_fetch_array($result))  
                           {  
                              echo '<option id="test_id" value="'.$row["client_id"].'">'.$row["name"].' ('.$row["email"].')</option>';  
                           }  
                       ?> 
                       </select>
                    
                    <!-- <td><input type="text" id="name" name="name" placeholder="name"></td> -->
                    <label>Client's address:</label>
                    <input type="text" id="address" name="address" placeholder="address">
                    <label>Place of supply:</label>
                    <input type="text" id="place_of_supply" name="place_of_supply" placeholder="place_of_supply">
                    <label>Client's state code:</label>
                    <input type="text" id="code" name="code" placeholder="code">
                    <button align="right" class="btn btn-primary" id="add_project">Add Project</button><br><br>
               
               <table class="table invoice_tax">
               <tr>
                  <td colspan="7"><h3><b>Project Details</b></h3></td>
               </tr>
               <tr>
                 <th colspan="2">project name</th>
                 <th>project description</th>
                 <th>hour based?</th>
                 <th>how many hours</th>
                 <th>rate</th>
                 <th>total</th>
               </tr>
               <tr class="project_details">
                 <td colspan="2"><select class="project_id" name="project_id[]" ></select></td>
                 <td><input type="text" id="project_description" name="project_description[]" placeholder="project_description"></td>
                 <td><input type="text" id="is_hourly_based" name="is_hourly_based[]" placeholder="is_hourly_based" readonly></td>
                 <td><input type="number" class="hours_count" name="hours_count[]" placeholder="hours_count" value="0" ></td>
                 <td><input type="number" class="Rate" id="Rate" name="rate[]" placeholder="Rate" required></td>
                 <td><input type="text" id="total" class="common_total" name="total[]" placeholder="total" ></td>
               </tr>              
            </tbody>

            <tbody id="tax_amount">
               
            </tbody>

            <tbody>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td><input type="text" id="currency" class=""></td>
               <td><input type="text" id="with_tax" class="" name="total_with_tax" placeholder="withtax total" ></td>
            </tbody>   

            <tbody>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>  
               <td><button type="submit" class="btn btn-success" name="download" id="download">Download</button></td>
               <td><button type="submit" class="btn btn-success" name="save_download" id="save_download">save & download</button></td>
            </tbody>   
    
      </table>
      </form>
  </div>
</div>

<script src="../assets/js/main.js"></script>
<script>
  $("#datepicker").datepicker();
  $("#invoice_date").datepicker();

//   var from = $("#invoice_date").val();
//   var to = $("#datepicker").val();

// $("#datepicker").change(function(){
//   if(Date.parse(from) > Date.parse(to)){
//      alert("Invalid Date Range");
//      $("#datepicker").val(" ");
//   }
//   else{
//      alert("Valid date Range");
//   }
// });
</script>
<script>
  $(document).ready(function(){
    $("#client_details_invoice").validate({
      rules:{
        inovice_number:{
          required:true
        },
        invoice_date:{
          required:true
        },
        due_date:{
          required:true
        },
        client_id:{
          required:true
        },
        name:{
          required:true
        },
        hours_count:{
          required:true,
          digits: true
        },
        address:{
          required:true
        },
        place_of_supply:{
          required:true
        },
        rate:{
          required:true,
          digits: true
        },
        total:{
          required:true,
          digits: true
        },
        code:{
          required:true
        },
        total_with_tax:{
          required:true,
          digits: true
        }
      }
    })
  })


</script>
</html>