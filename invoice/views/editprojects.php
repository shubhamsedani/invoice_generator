<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
?>

<html>
<head>
	<title>Edit Projects</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<!-- jquery cdn -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<style>
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>

<div class="createclients-page">
  <div class="form style">
    <div class="login-form">
    	<form id='editproject' method='post'>
		      <h3><b>Edit projects</b></h3>
		      <select id="client_id" name="client_id" hidden>
			    <?php  
				    $sql = "SELECT client_id FROM clients";
					$result = mysqli_query($conn, $sql);
					while($row = mysqli_fetch_array($result))  
					{  
					   echo '<option value="'.$row["client_id"].'">'.$row["client_id"].'</option>';  
					}  
		        ?> 
			  </select>
			  <?php 
					$id=base64_decode($_GET['project_description']);
				    if (isset($id)) {
						$selectquery = "select * FROM projects WHERE project_description = '". $id ."'";
						$query = mysqli_query($conn, $selectquery);
						$result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
						if (is_array($result_arr) || is_object($result_arr))
						{
							foreach ($result_arr as $row) {
				?>
			  <input type="text" value="<?=$row['client_name']?>" name='client_name' id="client_name">
		      <input type="text" value="<?=$row['project_name']?>" name='project_name' id="project_name">
		      <input type="text" value="<?=$row['project_description']?>" name='project_description' id="project_description">     

		      <select id="work_type" name="work_type">
			    <option value="no" <?=$row['is_hourly_based'] == '0' ? ' selected="selected"' : '';?>>No its not hour based</option>
			    <option value="yes" <?=$row['is_hourly_based'] == '1' ? ' selected="selected"' : '';?>>Yes its hour based</option>
			  </select>

		      <input type="text" value="<?=$row['rate_per_hour']?>" name='rate_per_hour' class="rate_per_hour" id="rate_per_hour">
		      <?php }}}?> 
		      <br><br>
		      <input type='submit' id="custom_submit" value='Save Projects'>
      </form>   
    </div>
  </div>
</div>

<script src="../assets/js/main.js"></script>
<script>

	$(document).ready(function(){
		if($('#work_type option').filter(':selected').text() == 'yes'){
			  $("#rate_per_hour").css("display", "block");
		}else{
			$("#rate_per_hour").css("display", "none");
		}
	});

	var c_id = '<?php echo $id; ?>';
	$('#editproject').submit(function(e){
	  e.preventDefault();
	  if($(this).valid()){
	  $.ajax({
	      type: "POST",
	      url: '../backend/crud/editprojects.php',
	      data: $(this).serialize(),
	      dataType:'json',
	      success: function( response ) {
	          window.location.href = "showprojects.php";
	      }
	  });
	  return false;
	}
	});
</script>

<script>
	$(document).ready(function(){
		$("#editproject").validate({
			rules:{
				project_name:{
					required:true
				},
				project_description:{
					required:true
				}
				
			}
		})
	})
</script>

</html>