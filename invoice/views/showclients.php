<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM clients where `delete_status`='not delete'";
$result = mysqli_query($conn, $sql);
?>

<html>
<head> 
	<title>Show clients</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
</head>

<body>
	<div class="show_clients_custom">
		<div >
			<a href="Createclients.php"><button align="right" class="btn btn-primary" id="add_client">Add Client</button></a>
			<a href="delete_clients.php"><button align="right" class="btn btn-danger" id="deleted">Deleted Client</button></a>
		</div>
				<?php
					if (isset($_SESSION['client_status'])){
					    echo "<span class='alert alert-danger'>" . $_SESSION['client_status'] . "</span><br><br>";
					}

				   if(isset($_SESSION['client_delete_status'])){
				   		echo "<span class='alert alert-danger'>" . $_SESSION['client_delete_status'] . "</span><br><br>";
				   } 

				   if(isset($_SESSION['edit_status'])){
				   		echo "<span class='alert alert-danger'>" . $_SESSION['edit_status'] . "</span><br><br>";
				   } 
			   ?>
	  <table id="myTable" border="1px">
	    <thead>
	      <tr>
	      	  <th>NO</th>
	          <th style="display: none;">CLIENT ID</th>
	          <th>NAME</th>
	          <th>EMAIL ID</th>
	          <th>PHONE NO</th>
	          <th>GST NUMBER</th>
	          <th>ADDRESS</th>
	          <th>PLACE OF SUPPLY</th>
	          <th>STATE_CODE</th>
	          <th>CURRENCY</th>
	          <th>Update</th>
	          <th>Delete</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  
			   echo '  
					   <tr>  
					   		<td>'.$i++.'</td>
					        <td style="display: none;">'.$row["client_id"].'</td>  
					        <td>'.$row["name"].'</td>  
					        <td>'.$row["email"].'</td>
					        <td>'.$row["phone_number"].'</td>
					        <td>'.$row["gst_number"].'</td> 
					        <td>'.$row["address"].'</td> 
					        <td>'.$row["place_of_supply"].'</td>  
					        <td>'.$row["state_code"].'</td>
					        <td>'.$row["currency"].'</td>
					        <td><button class="btn btn-warning" id="edit"><a href="updateclients.php?client_id='.base64_encode($row["client_id"]).'">Edit</a></button></td>
					        <td><button class="btn btn-danger delete" id="delete_row">Delete</button></td>  
					   </tr>  
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>


<script>
$(document).ready( function () {
    $('#myTable').DataTable({
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: true,
    columnDefs: [
            { width: 100 }
        ],
    fixedColumns: true
  });
} );
</script>

<?php unset($_SESSION['client_delete_status']); ?> 
<?php unset($_SESSION['client_status']); ?>
<?php unset($_SESSION['edit_status']); ?>
</html>
