<?php 
include "../conf/conn.php";
include '../shared/navigation.php';

$sql = "SELECT * FROM clients where `delete_status`='deleted'";
$result = mysqli_query($conn, $sql);
?>

<html>
<head> 
	<title>Deleted clients</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
	<!-- style CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<link rel="stylesheet" href="../assets/css/style.css">
</head>

<body>
	<div class="custom">
		<div ><button align="right" id="add_client"></button></div>
				<?php
				   if(isset($_SESSION['client_delete_status'])){
				   		echo "<span class='alert alert-danger'>" . $_SESSION['client_delete_status'] . "</span><br><br>";
				   } 
			   ?>
	  <table id="myTable" border="1px">
	    <thead>
	      <tr>
	      	  <th>NO</th>
	          <th style="display: none;">CLIENT ID</th>
	          <th>NAME</th>
	          <th>EMAIL ID</th>
	          <th>PHONE NO</th>
	          <th>GST NUMBER</th>
	          <th>ADDRESS</th>
	          <th>PLACE OF SUPPLY</th>
	          <th>STATE_CODE</th>
	          <th>CURRENCY</th>
	          <th>Restore</th>
	      </tr>
	    </thead>

	    <?php  
	    $i=1;
			while($row = mysqli_fetch_array($result))  
			{  
			   echo '  
					   <tr>  
					   		<td>'.$i++.'</td>
					        <td style="display: none;">'.$row["client_id"].'</td>  
					        <td>'.$row["name"].'</td>  
					        <td>'.$row["email"].'</td>
					        <td>'.$row["phone_number"].'</td>
					        <td>'.$row["gst_number"].'</td> 
					        <td>'.$row["address"].'</td> 
					        <td>'.$row["place_of_supply"].'</td>  
					        <td>'.$row["state_code"].'</td>
					        <td>'.$row["currency"].'</td>
					        <td><button class="btn btn-warning restore_client" id="restore_client">Restore</button></td> 
					   </tr>  
			   ';  
			}  
        ?>  
	  </table>
	</div>
</body>


<script>
$(document).ready( function () {
    $('#myTable').DataTable({
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: true,
    columnDefs: [
            { width: 100 }
        ],
    fixedColumns: true
  });
});
document.getElementById('add_client').style.visibility='hidden';
</script>

<script src="../assets/js/main.js"></script>
<?php unset($_SESSION['client_delete_status']); ?>
</html>
