<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
// session_start();
// print_r($_SESSION);
?>

<html>
<head>
	<title>Create clients</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->
	<!-- currency cdn	 -->
  	<link rel="stylesheet" href="../assets/pidie-master/pidie-0.0.8.css"/>
	<!-- jquery cdn -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<!-- jquery validation -->
   <style>
   label {display:block; width:x; height:y; text-align:left;}
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script> 
</head>


<div class="createclients-page">
  <div class="form style">
    <div class="login-form">
    	<form id='CreateClients' method='post'>
		      <h3><b>create client</b></h3>
		      <span class="required">Every feilds are required</span>
		      <label>Enter name:</label>
		      <input type="text" placeholder="Name" name='name' id="Name">
		      <label>Enter email:</label>
		      <input type="text" placeholder="email" name='email' id="email">
		      <label>Enter phone number:</label>
		      <input type="text" placeholder="phone number" name='phone_no' id="phone_no">
		      <label>Enter GST number:</label>
		      <input type="text" placeholder="Gst Number" name='gst_no' id="gst_no">
		      <label>Enter Address:</label>
		      <input type="text" placeholder="Address" name='address' id="Address">  
		      <label>Place of supply:</label>    
		      <input type="text" placeholder="Place of Supply" name='place_of_supply' id="Place_of_Supply">
		      <label>State code:</label>
		      <input type="text" placeholder="State code" name='state_code' id="State_code">
		     <!--  <div>
		      	<label for="country">Select list (select one):</label>
		      	<SELECT class="pd-countries form-control" name='country' id="country"></SELECT>
		      </div> -->
		      <div>
		      	<label>Select currency:</label>
		      	<SELECT class="pd-currencies form-control" name='currency' id="currency"></SELECT>
		      </div>
		      <h3><b>create taxes</b></h3>
		      <hr>
		      <div class="taxes">
		      <input type="text" placeholder="Tax name" name="tax_name[]" class="Tax_name" required>
		      <input type="text" placeholder="Percentage" name="tax_percentage[]" class="tax_Percentage" required>
		      <hr>
		      </div>
		      <div class="multiple_tax"></div>
		      <br><br>
		      <!-- <button name="cancel" style="max-width: 340px" id="add_taxes">Add More Taxes</button> -->
		      <br><br>
		      <input type='submit' id="custom_submit" value='Register client'>
      </form>
	  <button style="max-width: 340px" onclick="addtax()">Add More Taxes</button>
    </div>
  </div>
</div>

<script src="../assets/js/main.js"></script>

<script>
	$(document).ready(function(){
		$("#CreateClients").validate({
			rules:{
				name:{
					required:true
				},
				address:{
					required:true
				},
				email:{
					required:true
				},
				phone_no:{
					required:true,
					digits: true
				},
				gst_no:{
					required:true
				},
				place_of_supply:{
					required:true
				},
				currency:{
					required:true
				},
				state_code:{
					required:true
				},
				tax_percentage:{
					required: true,
      				digits: true
				}
			}
		})
	})
</script>
<script src="../assets/pidie-master/pidie-0.0.8.js"></script>
  <script>
  	new Pidie();
  </script>
</html>