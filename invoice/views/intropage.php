<?php 
include "../conf/conn.php";
include '../shared/navigation.php';	
?>


<html>
<head> 
	<title>Invoice System</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/IntroPage.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
	<!-- jquery cdn --> 
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
</head>

<body>
	<div class="custom">
		<div class="wrapper">
			  <div class="snone">
				<a href="#"><button align="right" id="add_client">Refresh Page</button></a>
			  </div>
		  <h1>Welcome you have</h1>
		  <div class="team">
		    <div class="counts">
		      <h3>Active Clients</h3>
		      <?php 
		      		$result = mysqli_query($conn, "select * from clients as a WHERE a.delete_status = 'not delete'");
					$num_rows = mysqli_num_rows($result);
		      ?>
		      <p class="role"><b><?php echo "$num_rows clients\n"; ?></b></p>
		    </div>

		    <div class="counts">
		      <h3>Active Projects</h3>
		      <?php 
		      		$result1 = mysqli_query($conn, "select * from projects as a WHERE a.delete_status = 'not delete'");
					$num_rows1 = mysqli_num_rows($result1);
		      ?>
		      <p class="role"><b><?php echo "$num_rows1 projects\n"; ?></b></p>
		    </div>

		    <div class="counts">
		      <h3>Invoices</h3>
		      <?php 
		      		$result2 = mysqli_query($conn, "select * from invoices as a WHERE a.delete_status = 'not delete'");
					$num_rows2 = mysqli_num_rows($result2);
		      ?>
		      <p class="role"><b><?php echo "$num_rows2 invoices\n"; ?></b></p>
		    </div>
		  </div>
		</div>
	</div>
</body>
</html>
