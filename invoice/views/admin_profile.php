<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
?>

<html>
<head>
   <title>Edit clients</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <!-- style CSS -->
   <link rel="stylesheet" href="../assets/css/dashboard.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
   <!-- jquery cdn -->
   <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <!-- jquery validation -->
   <style>
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>

<div ><a href="#"><button align="right" class="btn btn-success" id="mtop_space">Refresh page</button></a></div>
<!-- mtop-space is written to leave space -->

  <div class="form">
    <div class="login-form">
       <?php 
       if(isset($_SESSION['admin_update_status'])){
          echo "<span class='alert alert-danger'>" . $_SESSION['admin_update_status'] . "</span><br><br>";
       } 
     ?>
           <h3><b>Company details</b></h3>
            <form id="admin_details_invoice">
            <table class="table">
               <?php 
                  $selectquery = "select * FROM company_details";
                  $query = mysqli_query($conn, $selectquery);
                  $result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
                  if (is_array($result_arr) || is_object($result_arr))
                  {
                     foreach ($result_arr as $row) {
               ?>

               <tbody>
                <tr>
                 <td rowspan="2"><img src="../assets/images/weboccult.jfif" width="120" height="120"></td>
                 <td colspan="5"><input type="text" value="<?=$row['company_full_title']?>" name='company_full_title'></td>
                 <td><h4>Invoice</h4></td>   
               </tr>
               <tr>
                 <td colspan="3"><input type="text" value="<?=$row['website_address']?>" name='website_address'></td>
                 <td colspan="2"><input type="text" value="<?=$row['mail_address']?>" name='mail_address'></td>
                 <td><h4>TAX Invoice</h4></td>   
               </tr>
             </tbody>
             <div></div>

             <tbody>
                <tr>
                  <td><h4>Company address:</h4></td>
                 <td colspan="6"><input type="text" value="<?=$row['company_address']?>" name='company_address'></td>
               </tr>
             </tbody>

             <tbody>
                <tr>
                  <td><h4>CIN number:</h4></td>
                  <td colspan="3"><input type="text" value="<?=$row['CIN_number']?>" name='CIN_number'></td>
                  <td></td>
                  <td colspan="2"></td>
               </tr>
               <tr>
                  <td><h4>PAN number:</h4></td>
                  <td colspan="3"><input type="text" value="<?=$row['PAN_number']?>" name='PAN_number'></td>
                  <td></td>
                  <td colspan="2"></td>
               </tr>
               <tr>
                  <td><h4>GST number:</h4></td>
                  <td colspan="3"><input type="text" value="<?=$row['GST_number']?>" name='GST_number'></td>
                  <td></td>
                  <td colspan="2"></td>
               </tr>
               <tr>
                  <td><h4>IEC number:</h4></td>
                  <td colspan="3"><input type="text" value="<?=$row['IEC_number']?>" name='IEC_number'></td>
                  <td></td>
                  <td colspan="2"></td>
               </tr>
             </tbody>

             <tbody>
                <tr>
                  <td><h4>Bank Details:</h4></td>
                 <th colspan="3"><input type="text" value="<?=$row['company_full_title']?>" name='company_full_title'></th>
               </tr>
               <tr>
                  <td><h4>Bank Name</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['Bank_name']?>" name='Bank_name'></td>
                  <td colspan="4"></td>
               </tr>
               <tr>
                  <td><h4>INR account</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['INR_account_number']?>" name='INR_account_number'></td>
                  <td colspan="4"><h4>Note</h4></td>
               </tr>
               <tr>
                  <td><h4>Euro account</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['EURO_account_number']?>" name='EURO_account_number'></td>
                  <td colspan="4"><input type="text" value="<?=$row['note_1']?>" name='note_1'></td>
               </tr>
               <tr>
                  <td><h4>Routing number</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['Routing_number']?>" name='Routing_number'></td>
                  <td colspan="4"><input type="text" value="<?=$row['note_2']?>" name='note_2'></td>
               </tr>
               <tr>
                  <td><h4>Swift Code</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['swift_code']?>" name='swift_code'></td>
                  <td colspan="4"><input type="text" value="<?=$row['note_3']?>" name='note_3'></td>
               </tr>
               <tr>
                  <td><h4>Mobile Number</h4></td>
                  <td colspan="2"><input type="text" value="<?=$row['mobile_number']?>" name='mobile_number'></td>
                  <td colspan="4"></td>
               </tr>
            </tbody>
             
             <tbody>
                <tr>
                  <td><h4>Thank you</h4></td>
                  <td colspan="3"><input type="text" value="<?=$row['thank_you_message']?>" name='thank_you_message'></td>
               </tr>
               <tr>
                   <td><h4>Director name</h4></td>
                   <td colspan="3"><input type="text" value="<?=$row['first_name']?>" name='director_name'></td>
               </tr>
               <tr>
                    <td><h4>Director surname</h4></td>
                   <td colspan="2"><input type="text" value="<?=$row['last_name']?>" name='director_lastname'></td>
               </tr>
            </tbody>
           <?php }}?>  
      </table>
      <button type="submit" class="btn btn-success" name="update_admin" id="update_admin">Update Admin</button>
      </form>
  </div>
</div>

<script src="../assets/js/main.js"></script>
<script>
  $("#admin_details_invoice").submit(function(e){
    e.preventDefault();
    e.stopPropagation(); 
    $.ajax({
           type: "POST",
           url: '../backend/crud/update_admin.php',
           dataType:'JSON',
           data: $(this).serialize(),
           success: function( response ){
             location.reload(); 
            }
        });
});


  $(document).ready(function(){
    $("#admin_details_invoice").validate({
      rules:{
        company_full_title:{
          required:true
        },
        website_address:{
          required:true
        },
        mail_address:{
          required:true
        },
        company_address:{
          required:true
        },
        CIN_number:{
          required:true
        },
        inovice_number:{
          required:true
        },
        PAN_number:{
          required:true
        },
        invoice_date:{
          required:true
        },
        GST_number:{
          required:true
        },
        due_date:{
          required:true
        },
        IEC_number:{
          required:true
        },
        Bank_name:{
          required:true
        },
        INR_account_number:{
          required:true
        },
        Bank_name:{
          required:true
        },
        EURO_account_number:{
          required:true
        },
        note_1:{
          required:true
        },
        Routing_number:{
          required:true
        },
        note_2:{
          required:true
        },
        swift_code:{
          required:true
        },
        note_3:{
          required:true
        },
        mobile_number:{
          required:true
        },
        thank_you_message:{
          required:true
        },
        director_name:{
          required:true
        },
        client_id:{
          required:true
        },
        name:{
          required:true
        },
        address:{
          required:true
        },
        place_of_supply:{
          required:true
        },
        rate:{
          required:true,
          digits: true
        },
        total:{
          required:true,
          digits: true
        },
        code:{
          required:true
        }
      }
    })
  })


</script>
<?php unset($_SESSION['admin_update_status']); ?>
</html>