<?php 
include "../conf/conn.php";
include '../shared/navigation.php';
?>

<html>
<head>
	<title>Edit clients</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- style CSS -->
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" href="../assets/pidie-master/pidie-0.0.8.css"/>
	<!-- jquery cdn -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<style>
    .error{
      color: red;
    }
  </style>
  <script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
</head>

<?php 
	$id=base64_decode($_GET['client_id']);
    if (isset($id)) {
		$selectquery = "select * FROM clients WHERE client_id = ". $id;
		$query = mysqli_query($conn, $selectquery);
		$result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
		if (is_array($result_arr) || is_object($result_arr))
		{
			foreach ($result_arr as $row) {
?>

<div ><a href="#"><button align="right" class="btn btn-success" id="mtop_space">Refresh page</button></a></div>
<!-- mtop-space is written to leave space -->
  <div class="form">
    <div class="login-form">
    	<form id='EditClients' method='post'>
    	    		<input type="hidden" value="<?=$row['currency']?>" name='currency' id="currency"  >
		      <h3><b>Edit client</b></h3>
		      <table class="table">
			    <thead>
			      <tr>
			        <th>Name</th>
			        <th>Email</th>
			        <th>Phone No.</th>
			        <th>GST No</th>
			       </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td><input type="text" value="<?=$row['name']?>" name='name' id="name"  ></td>
			        <td><input type="text" value="<?=$row['email']?>" name='email' id="email"  ></td>
			        <td><input type="text" value="<?=$row['phone_number']?>" name='phone_no' id="phone_no"></td>
			        <td><input type="text" value="<?=$row['gst_number']?>" name='gst_no' id="gst_no"  ></td>
			      </tr>
			      <!-- <thead> -->
			      	<tr>
			        <th>Address</th>
			        <th>Place of Supply</th>
			        <th>State_code</th>
			        <th>Currency</th>
			      </tr>
			      <!-- </thead> -->
			      <tr>
			        <td><input type="text" value="<?=$row['address']?>" name='address' id="address"  >  </td>
			        <td><input type="text" value="<?=$row['place_of_supply']?>"  name='place_of_supply' id="place_of_supply"></td>
			        <td><input type="text" value="<?=$row['state_code']?>" name='state_code' id="state_code"></td>
			        <td>
			        	<SELECT class="pd-currencies form-control" name='currency' id="currency">
			        		<option value="<?=$row['currency']?>"><?=$row['currency']?></option> 
			        	</SELECT>
				    </td>
			        <!-- <td><input type="text" value="<?=$row['currency']?>" name='currency' id="currency"  ></td> -->
			      </tr>
			    </tbody>
			  </table>
			  <input type='submit' id="edit_client" value='update client'>
		</form>
		      
		      <?php }}}?>
		      <form id='Edit_tax' method='post'>
		      <h3><b>Edit taxes</b></h3>
		      
		      <!-- <button align="left"class="btn btn-primary">Change</button></div> -->
		      <table class="table">
					<thead>
						<tr>
							<th>Tax Type</th>
							<th>Tax(in %)</th>
							<th>Action</th>
						</tr>
					</thead>
						<tbody>

		      <?php 
			    if (isset($id)) {
					$selectquery = "select * FROM client_taxes WHERE client_id = ". $id;
					$query = mysqli_query($conn, $selectquery);
					$result_arr = mysqli_fetch_all ($query, MYSQLI_ASSOC);
					if (is_array($result_arr) || is_object($result_arr))
						{ 
						foreach ($result_arr as $row) {
				?>
						<tr>
							<td><input type="text" value="<?=$row['tax_name']?>" name="tax_name[]" class="tax_name" required></td>
							<td><input type="text" value="<?=$row['percentage']?>" name="tax_percentage[]" class="tax_Percentage" required></td>
							<td>
								<button class="btn btn-danger delete_tax" id="delete_tax">Delete</button>
							</td>
						</tr>      
					
				<?php }}}?> 

					</tbody>
				</table>
				<div ><button align="right" class="btn btn-success" id="addtax_edit">Add Tax</button><br><br>
				<input type='submit' id="edit_tax" value='update tax'>
			</form>
         
    </div>
  </div>
</div>

<script src="../assets/pidie-master/pidie-0.0.8.js"></script>
 <script>
  	new Pidie();
  </script>
<script src="../assets/js/main.js"></script>
<script>

	var c_id = '<?php echo $id; ?>';
	$('#EditClients').submit(function(e){
		if($(this).valid()){
	  e.preventDefault();
	  $.ajax({
	      type: "POST",
	      url: '../backend/crud/editclients.php',
	      data: $(this).serialize() + "&client_id="+c_id,
	      dataType:'json',
	      success: function( response ) {
	          window.location.href = "showclients.php";
	      }
	  });
	  return false;
	}
	});

	$('#Edit_tax').submit(function(e){
	  e.preventDefault();
	  window.location.href = "showclients.php";
	  if($(this).valid()){
	  $.ajax({
	      type: "POST",
	      url: '../backend/crud/edit_taxs.php',
	      data: $(this).serialize() + "&client_id="+c_id,
	      dataType:'json',
	      success: function( response ) {
	          // location.reload();
	      }
	  });
	  return false;
	}
	});

</script>

<script>
	$(document).ready(function(){
		$("#EditClients").validate({
			rules:{
				state_code:{
					required:true
				},
				address:{
					required:true
				},
				place_of_supply:{
					required:true
				},
				name:{
					required:true
				},
				phone_no:{
					required:true,
					digits: true
				},
				gst_no:{
					required:true
				},
				currency:{
					required:true
				}
			}
		})
	})
</script>
</html>


