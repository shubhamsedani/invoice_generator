<?php

include '../../conf/conn.php';

$name = $_POST["name"];
$email = $_POST["email"];
$phone_no = $_POST["phone_no"];
$gst_no = $_POST["gst_no"];
$address = $_POST["address"];
$place_of_supply = $_POST["place_of_supply"];
$state_code = $_POST["state_code"];
$currency = $_POST["currency"];
session_start();


$check_edit = "SELECT * FROM clients WHERE email = ?"; // SQL with parameters
$stmt = $conn->prepare($check_edit); 
$stmt->bind_param("s", $email);
$stmt->execute();
$rs = $stmt->get_result(); // get the mysqli result
$row = $rs->fetch_assoc();

	if(count(array($row)) > 1) {
	    $response = [];
	    $response['status'] = 0;
		$response['message'] = 'there is a client with same email';
		$_SESSION['client_status'] = 'there is a client with same email';
		echo json_encode($response); 
	}else{
		// $sql = "insert into clients(name,email,	phone_number,gst_number,address,place_of_supply,state_code,currency,delete_status) values( ?, ? , ? , ? ,? , ? , ? , 'not delete')";
		// $stmt = $conn->prepare($sql); 
		// $stmt->bind_param("ssssssss", $name, $email, $phone_no, $gst_no, $address, $place_of_supply, $state_code, $currency);
		// $stmt->execute();


		$sql = "insert into clients(name,email,	phone_number,gst_number,address,place_of_supply,state_code,currency,delete_status) values('".$name."','".$email."','".$phone_no."','".$gst_no."','".$address."','".$place_of_supply."','".$state_code."','".$currency."', 'not delete')";
		// echo $sql;
		$response = [];//array stores the value for json reponse
		if(!$conn->query($sql)){
			$response['status'] = 0;
			$response['message'] = 'Already exist with this email.';
			$_SESSION['client_status'] = 'Already exist with this email.';
		}
		else{
			$response['status'] = 1;
			$response['message'] = 'Client added.';
			$_SESSION['client_status'] = 'Client '.$name.' added';
		}
		echo json_encode($response); 
	}


$sql3 = 'SELECT max(client_id) as c_id from clients';
$result = $conn -> query($sql3);
if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
		for ($i=0; $i < count($_POST['tax_name']); $i++) { 
			$sql = "insert into client_taxes(client_id, tax_name,percentage) values('".$row['c_id']."','".$_POST['tax_name'][$i]."','".$_POST['tax_percentage'][$i]."')";	
			$conn->query($sql);	
		}

	}
}

$stmt->close();
$conn->close();
?>