<?php
//include the db connection file
include "../../conf/conn.php";

//get username and password from the login page
$UserName = $_POST["UserName"];
$Password = $_POST["Password"];


//with sql injection
$sql = "select * from users where email= ? and password = ?"; // SQL with parameters
$stmt = $conn->prepare($sql); 
$stmt->bind_param("ss", $UserName, $Password);
$stmt->execute();
$result = $stmt->get_result(); // get the mysqli result
$user = $result->fetch_assoc();


//without sql injection
// $sql = "select * from company_details where login_email='".$UserName."' and login_password='".$Password."'";	
// $result=mysqli_query($conn,$sql);
	$response = [];//array stores the value for json reponse
	if(mysqli_num_rows($result)==1){
		session_start();
		$_SESSION["firstname"]=$UserName;
		$response['status'] = 1;
		$response['message'] = 'User found.';
	}else{
		$response['status'] = 0;
		$response['message'] = 'User not found.';
	}
	echo json_encode($response);

$stmt->close();
$conn->close();

?>