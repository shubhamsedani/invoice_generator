//ADD TAX IN CREATE CLIENT PAGE {ARRAY USED IN NAME SO WE CAN GET MULTIPLE VALUE OF DOM ELEMENT}
function addtax(){
$test = "<input type='text' placeholder='Tax name' class='Tax_name' name='tax_name[]' required> <input type='text' placeholder='Percentage' name='tax_percentage[]' class='tax_Percentage' required > <hr>";
	$(".multiple_tax").append($test);
}

//CREATE CLIENT PAGE SUBMISSION 
//IT WILL GIVE DATA TO API AND GET TO THE SHOW CLIINT PAGE
$('#CreateClients').submit(function(e){
	e.preventDefault();
	if($(this).valid()){
    	$.ajax({
	      type: "POST",
	      url: '../backend/crud/createclients.php',
	      data: $(this).serialize(),
	      dataType:'json',
	      success: function( response ) {
	          if(response.status == 0){
					window.location.href = "showclients.php";
	          }else{
	          		window.location.href = "showclients.php";
	          }  
	      }
	  	});
  		return false;
  	}
});

//DELETE CLIENT ON SHOW CLIENT PAGE
//IT WILL FIRST USE AJAX CALL TO GET INVOIVE NUMBER OF THAT CLIENT 
//AFTER COUNTING OF NUMBER OF INVOICE IT WILL USE SWEET ALERT BEFORE DELETING CLINT
$(document).ready(function(){
	$(".delete").click(function(){
		var Client_id = $(this).parent().siblings(":first").next().text();
		var Client_name = $(this).parent().siblings(":first").next().next().text();
		$.ajax({
		      type: "POST",
		      url: '../backend/crud/showtable.php',
		      data: { Client_id:Client_id, Client_name:Client_name, op:"info"},
		      dataType:'json',
		      success: function(results) {
		           swal({
						  title: "Are you sure?",
						  text: Client_name+ " is having " +results.data.length+ " active projects and " +results.data1.length+ " invoices!",
						  icon: "warning",
						  buttons: true,
						  dangerMode: true,
						})
						.then((willDelete) => {
						  if (willDelete) {
						    swal("Poof! Your imaginary file has been deleted!", {
						      icon: "success",
						    });
							$(this).parent().parent().remove();
							$.ajax({
						      type: "POST",
						      url: '../backend/crud/showtable.php',
						      data: { Client_id:Client_id, Client_name:Client_name, op:"delete"},
						      dataType:'json',
						      success: function( response ) {
						           if(response.status == 0){
											location.reload();
							          }else{
							          		location.reload();
							          }  
						      }
						  });
						  } else {
						    swal("Your imaginary file is safe!");
						  }
						});
		      }
		  });	
	});
});

//ADD TAX ON UPDATECLIENTS PAGE
//prevent default is must to avoid duplication
$('#addtax_edit').on( 'click', function(e) {
	e.preventDefault();
	$('#Edit_tax tbody').append('<tr><td><input type="text" name="tax_name[]" class="tax_name"></td><td><input type="text" name="tax_percentage[]" class="tax_Percentage"></td><td><button class="btn btn-danger delete_tax" id="delete_tax">Delete</button></td></tr>');
});

//DELETE TAX ON UPDATECLIENTS PAGE
$('.delete_tax').on( 'click', function(e) {
	e.preventDefault();
	$(this).parent().parent().remove();
});


//CREATE PROJECT PAGE SUBMISSION 
//IT WILL GIVE DATA TO API AND GET TO THE SHOW PROJECT PAGE
$('#Createproject').submit(function(e){
  e.preventDefault();
  var client_idd = $('#client_id option:selected').data('attr');
  if($(this).valid()){
		$.ajax({
	      type: "POST",
	      url: '../backend/crud/createprojects.php',
	      data: $(this).serialize() + "&client_idd='" +client_idd+ "'",
	      dataType:'json',
	      success: function( response ) {
	          if(response.status == 0){
						window.location.href = "showprojects.php";
		          }else{
		          		window.location.href = "showprojects.php";
		          }  
	      }
	  });
		return false;
  }
});

//DELETE CLIENT ON SHOW PROJECT PAGE
//IT WILL FIRST USE AJAX CALL TO GET INVOIVE NUMBER OF THAT PROJECT 
//AFTER COUNTING OF NUMBER OF INVOICE IT WILL USE SWEET ALERT BEFORE DELETING PROJECT
$(document).on('click', '#delete_project', function(e) {
e.preventDefault(); 
var project_name = $(this).parent().siblings(":first").next().next().next().text();
var client_id = $(this).parent().siblings(":first").next().text();
var remove = $(this).parent().parent();
$.ajax({
type: "POST",
url: '../backend/crud/deleteproject.php',
data: { project_name:project_name, client_id:client_id, op:"info"},
dataType:'json',
success: function(results) {
		swal({				  
			  title: "Are you sure?",
			  text:  "This project is having " +results.data1.length+ " invoices!",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			    swal("Poof! Your imaginary file has been deleted!", {
			      icon: "success",
			    });
				remove.remove(); 
				$.ajax({       
					type: "POST",
					url: '../backend/crud/deleteproject.php',       
					data: { project_name:project_name, op:"delete"},       
					dataType:'json',       
					success: function(response ) {           
						if(response.status == 0){ 
							location.reload();
						}else{
							location.reload();
						}        
					}   
				});
			  } else {
			    swal("Your imaginary file is safe!");
			  }
		});
		}
 	});
});

//GENERATE INVOICE PAGE 
//ON CHANGE OF FIRST SELECT CLIENT DROPDOWN IT WILL CALL API AND GET DATA OF CLIENT 
//FIRST AJAX CALL TO GET CLIENT DETAILS
//SECOND AJAX CALL TO GET THAT CLIENTS PROJECTS DETAILS
var c_id, project_id, currency;
$('#client_id').on('change', function(e) {
 c_id = this.value;
 e.preventDefault();
    $.ajax({
       type: "POST",
       url: '../backend/crud/invoice.php',
       dataType:'JSON',
       data: {client_id:c_id},
       success: function( response ){
           // $("#name").val(response.data[0].name);
           $("#address").val(response.data[0].address);
           $("#place_of_supply").val(response.data[0].place_of_supply);
           $("#code").val(response.data[0].state_code);
           $("#currency").val("Client Currency: " + response.data[0].currency);
       }
    });
    $.ajax({
       type: "POST",
       url: '../backend/crud/invoice_addproject.php',
       dataType:'JSON',
       data: {client_id:c_id},
       success: function( response ){
       	var options = '<option value="select_something">Select Project</option>';
       	if(response.success == "1"){
       		for(var i=0; i<(response.data).length; i++){
       			options += '<option value="' +response.data[i].project_name+'">'+response.data[i].project_name+'</option>';
       		}
       	}else{
       			options += '<option value="there is no data">there is no data</option>';
       	}
       	$('.project_id').html(options);
       	//suppose user add project and change the user name so you have to make empty the feilds of old selected user
       	options = " "; 
       	$(".invoice_tax").find("tr.project_details").eq(0).find('input').val('');
 		$(".invoice_tax").find("tr.project_details:gt(0)").remove();
       }
    });
return false;
});

//GENERATE INVOICE PAGE ADD MULTIPLE PROJECTS
$("#add_project").on('click', function(e) {
	e.preventDefault();
	var thisistest = $(".invoice_tax").find("tr").eq(2).clone();
	$(".invoice_tax").append(thisistest);
});

//GENERATE INVOICE PAGE ADD MULTIPLE PROJECTS
//THIS WILL BE SHOW THE DATA OF PROJECTS AFTER CLIENT WILL SELECT A PROJECT 
var des, type, hour_count, rate, total;
$(document).on('change', '.project_id', function(event){
 project_id = $(this).val();
 event.stopImmediatePropagation();
 des = $(this).closest('td').next('td').find('input');
 type = $(this).closest('td').next('td').next('td').find('input');
 hour_count = $(this).closest('td').next('td').next('td').next('td').find('input');
 rate = $(this).closest('td').next('td').next('td').next('td').next('td').find('input');
 total = $(this).closest('td').next('td').next('td').next('td').next('td').next('td').find('input');
    $.ajax({
       type: "POST",
       url: '../backend/crud/invoice_pricing.php',
       dataType:'JSON',
       data: {project_id:project_id},
       success: function( response ){
           des.val(response.data[0].project_description);
           rate.val(response.data[0].rate_per_hour);
           var total_price = response.data[0].rate_per_hour;
           if(response.data[0].is_hourly_based == 0){
           		type.val("No");
           		total.val(response.data[0].rate_per_hour);
           		hour_count.val("1");
           		hour_count.prop('readonly', true);
           		rate.prop('readonly', false);
           		// hour_count.prop('disabled', true);
           }else{
           	  type.val("Yes");
           	  // rate.prop('disabled', true);
           	  rate.prop('readonly', true);
           	  hour_count.prop('readonly', false);
           }
       }
    });
});	

//GENERATE INVOICE PAGE 
//INVOICE PAGE SUB TOTAL COUNT BASED ON HOUR 
$(document).on('change', '.hours_count', function(e){
// $(".hours_count").on('change', function() {
	e.preventDefault();
	var hours = this.value;
	var test = $('.common_total');
	var test1 = $('.common_tax');
	var with_tax = 0;
	var with_tax1 = 0;
	var without_tax = 0;
	var total_price = $(this).parent().next().find("input").val();
	$(this).parent().next().next().find("input").val(total_price*hours);

	//THIS FOR LOOP USED TO CHECK HOW MANY PROJECT TOTALS ARE THERE 
	for(var i=0; i<test.length; i++){
		without_tax += parseInt($(".common_total").eq(i).val());
	}

	//BASED ON THE NUMBER OF PROJECTS AND ITS TOTAL THIS WILL MAKE THE MAIN TOTAL
	for(var i=0; i<test1.length; i++){
		var random = (without_tax + (without_tax * parseInt($(".common_tax").eq(i).val()))/100);
		with_tax += random - without_tax;
		with_tax1 = random - without_tax;
		$(".tax_total").eq(i).val(with_tax1);
	}
	$("#with_tax").val((with_tax + without_tax));
});

//GENERATE INVOICE PAGE 
//INVOICE PAGE SUB TOTAL COUNT BASED ON NONE HOUR BASED PROJECT
$(document).on('change', '.Rate', function(e){
// $(".Rate").on('change', function() {
	e.preventDefault();
	var rates = this.value;
	var test = $('.common_total');
	var test1 = $('.common_tax');
	var with_tax = 0;
	var with_tax1 = 0;
	var without_tax = 0;
	$(this).parent().next().find("input").val(rates);
	//THIS FOR LOOP USED TO CHECK HOW MANY PROJECT TOTALS ARE THERE
	for(var i=0; i<test.length; i++){
		without_tax += parseInt($(".common_total").eq(i).val());
	}

	//BASED ON THE NUMBER OF PROJECTS AND ITS TOTAL THIS WILL MAKE THE MAIN TOTAL
	for(var i=0; i<test1.length; i++){
		var random = (parseInt(without_tax) + parseInt(without_tax * parseInt($(".common_tax").eq(i).val()))/100);
		with_tax += parseInt(random) - parseInt(without_tax);
		with_tax1 = parseInt(random) - parseInt(without_tax);
		$(".tax_total").eq(i).val(with_tax1);
	}
	$("#with_tax").val((parseInt(with_tax) + parseInt(without_tax)));
});


//if user change the tax percentage while generating the invoice
$(document).on('change', '.common_tax', function(e){
	e.preventDefault();
	var test = $('.common_total');
	var test1 = $('.common_tax');
	var with_tax = 0;
	var with_tax1 = 0;
	var without_tax = 0;
	//THIS FOR LOOP USED TO CHECK HOW MANY PROJECT TOTALS ARE THERE
	for(var i=0; i<test.length; i++){
		without_tax += parseInt($(".common_total").eq(i).val());
	}

	//BASED ON THE NUMBER OF PROJECTS AND ITS TOTAL THIS WILL MAKE THE MAIN TOTAL
	for(var i=0; i<test1.length; i++){
		var random = (parseInt(without_tax) + parseInt(without_tax * parseInt($(".common_tax").eq(i).val()))/100);
		with_tax += parseInt(random) - parseInt(without_tax);
		with_tax1 = parseInt(random) - parseInt(without_tax);
		$(".tax_total").eq(i).val(with_tax1);
	}
	$("#with_tax").val((parseInt(with_tax) + parseInt(without_tax)));
});

// $(document).on('change', '#client_id', function(e){
$('#client_id').on('change', function(e) {
 c_id = this.value;
 e.preventDefault();
    $.ajax({
       type: "POST",
       url: '../backend/crud/invoice_taxes.php',
       dataType:'JSON',
       data: {client_id:c_id},
       success: function( response ){
       		if(response.success == "1"){
       			var options;
	       		for(var i=0; i<(response.data).length; i++){
	       			options += '<tr><td colspan="2"></td><td></td><td></td><td><input type="text" name="tax_name[]" value="' +response.data[i].tax_name+'"></td><td><input type="text" name="tax_percentage[]" class="common_tax" value="' +response.data[i].percentage+'"></td><td><input type="text" name="tax_total[]" class="tax_total" readonly></td></tr>';
	       		}
	       	}
	       	$('#tax_amount').html(options);
	       	options = " ";
       	}
    });
});

//show feild to add rate per our if it is selected
$('#work_type').on('change', function() {
	var ourbased = this.value;
	if(ourbased == 'yes'){
		  $(".rate_per_hour").css("display", "block");
	}else{
		$(".rate_per_hour").css("display", "none");
	}
});

//restore projects
$(".restore").click(function(){
	var client_id = $(this).parent().siblings(":first").next().text();
	var project_description = $(this).parent().siblings(":first").next().next().next().next().text();
	$.ajax({
       type: "POST",
       url: '../backend/crud/restore_projects.php',
       dataType:'JSON',
       data: {project_description:project_description, client_id:client_id},
       success: function( response ){
       		location.reload(); 
       	}
    });
});

// restore_client
$(".restore_client").click(function(){
	var email_id = $(this).parent().siblings(":first").next().next().next().text();
	$.ajax({
       type: "POST",
       url: '../backend/crud/restore_clients.php',
       dataType:'JSON',
       data: {email_id:email_id},
       success: function( response ){
       		location.reload();
       	}
    });
});


//delete invoices
$(".invoice").click(function(){
	 swal({
		  title: "Are you sure?",
		  text: "You will never able to see this invoice!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		    swal("Poof! Your imaginary file has been deleted!", {
		      icon: "success",
		    });
					var invoice_id = $(this).parent().siblings(":first").next().next().text();
					$.ajax({
				       type: "POST",
				       url: '../backend/crud/delete_invoice.php',
				       dataType:'JSON',
				       data: {invoice_id:invoice_id},
				       success: function( response ){
				       		location.reload();
				       	}
				    });
		    } else {
				    swal("Your imaginary file is safe!");
				  }
				});
});

//restore invoice
$(".restore_invoice").click(function(){
	var invoice_id = $(this).parent().siblings(":first").next().next().text();
	$.ajax({
       type: "POST",
       url: '../backend/crud/restore_invoice.php',
       dataType:'JSON',
       data: {invoice_id:invoice_id},
       success: function( response ){
       		location.reload();
       	}
    });
});