<?php

session_start();	
if(!isset($_SESSION["firstname"])){
	?>
	<script>window.location.href = "../login.php";</script>
	<?php
}

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="../assets/css/main.css">
	<!-- jQuery cdn -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<!-- jQuery validation -->
	<script src="../jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>

	<style>
		.dropbtn {
		  background-color: #04AA6D;
		  color: white;
		  padding: 16px;
		  font-size: 16px;
		  border: none;
		}

		.dropdown {
		  position: relative;
		  display: inline-block;
		}

		.dropdown-content {
		  display: none;
		  position: absolute;
		  background-color: #f1f1f1;
		  min-width: 160px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  z-index: 1;
		}

		.dropdown-content a {
		  color: black;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		}

		.dropdown-content a:hover {background-color: #ddd;}

		.dropdown:hover .dropdown-content {display: block;}

		.dropdown:hover .dropbtn {background-color: #3e8e41;}

		table#myTable {
		    /*margin-top: 10%;*/
		    /*max-width: 0;*/
		}

		.logo img {
		    width: 25px;
		    height: 25px;
		}

		.logo span {
		    font-size: 18px;
		}

		.navbar-btn button {
		    font-size: 20px;
		    padding: 4px 7px;
		    line-height: .7;
		    border: none;
		    background: none;
		    outline: none;
		}

		.dropdown-content {
		    display: none;
		    position: absolute;
		    background-color: #f1f1f1;
		    min-width: 160px;
		    box-shadow: 0px 8px 16px 0px rgb(0 0 0 / 20%);
		    z-index: 1;
		    left: -45px;
		    top: 35px;
		}

		.dropdown-content a{
			border-bottom: 1px solid black;
		}

		</style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="../views/Dashboard.php" class="img-responsive logo"><img src="../assets/images/invoice.png"><span>     Invoice System</span></a>
				<!-- <a href="../views/Dashboard.php"><class="img-responsive logo"><span>Invoice System</span></a> -->
			</div>
			<div class="container-fluid">

				<div class="navbar-btn navbar-btn-right">
						<!-- <a href="../backend/auth/logout.php" class="btn btn-success update-pro" type="submit" id="logout"><i class="fa fa-rocket"></i> <span>logout</span></a> -->
					<div class="dropdown btn btn-success update-pro">
						<button class="dropbtn"><b>Profile</b></button>
						<div class="dropdown-content">
						    <a href="../views/user_profile.php">Edit Profile</a>
						    <a href="../backend/auth/logout.php">logout</a>
						 </div>
					</div>
				</div>
				<!-- <div class="navbar-btn navbar-btn-right">
						<a href="../views/admin_profile.php" class="btn btn-success update-pro" type="submit" id="Admin_Profile"><i class="fa fa-rocket"></i> <span>Admin Profile</span></a>
				</div> -->
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="../views/intropage.php" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li><a href="../views/Dashboard.php"><i class="lnr lnr-printer"></i> <span>Generate invoice</span></a></li>
						<li><a href="../views/showclients.php" class=""><i class="lnr lnr-plus-circle"></i> <span>Clients</span></a></li>
						<li><a href="../views/showprojects.php" class=""><i class="lnr lnr-plus-circle"></i> <span>Projects</span></a></li>
						<li><a href="../views/invoice.php" class=""><i class="lnr lnr-plus-circle"></i> <span>Invoices</span></a></li>
						<li><a href="admin_profile.php" class=""><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->


<script src="../assets/js/main.js"></script>
</body>
</html>
