# step-1:
* download invoice folder from the invoice_generator repository.

# step-2:
* before you start anything, first download database from the database folder and import into your localhost/phpmyadmin.

# step-3:
* now setup invoice folder into your locahost. (i.e. if you are using xampp then save invoice into htdocs folder).

# step-4:
* start your localhost and open invoice/login.php file from your browser.
